import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsChoice implements EventHandler<ActionEvent> {
    private final TextField message;
    private final TextField wins;
    private final TextField losses;
    private final TextField ties;
    private final String pChoice;
    private final RpsGame game;

    public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String PChoice, RpsGame game){

        this.message = message;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        pChoice = PChoice;
        this.game = game;
    }
    @Override
    public void handle(ActionEvent actionEvent) {
        String gameMessage = game.playRound(pChoice);
        this.message.setText(gameMessage);
        this.wins.setText("Wins: " + game.getWins());
        this.losses.setText("Losses: " + game.getLosses());
        this.ties.setText("Ties: " + game.getTies());

    }
}

// Parhomenco Kirill 1933830
import java.util.Arrays;
import java.util.Random;

public class RpsGame {
    private int wins = 0;
    private int ties = 0;
    private int losses = 0;
    private final int RPS = 3; // for random generator
    private final String[] RPS_CHOICE= {"rock","paper","scissors"};
    private final Random dice = new Random();
    private final int[] winCondition = {-1,-2,3};

    public int getWins() {
        return wins;  
    }

    public int getTies() {
        return ties;
    }

    public int getLosses() {
        return losses;
    }

    /**
     * Plays a round of the game and changes the fields based on outcome
     * @param playerChoice payer's input weapon
     * @return congratulations on winning/losing
     */
    public String playRound(String playerChoice){
        String AIChoice = RPS_CHOICE[dice.nextInt(RPS)];
        String winOrLose = determineWinner(playerChoice, AIChoice);
        return "Computer plays " + AIChoice + " and the computer " + winOrLose;
    }

    /**
     * Method returns a string representing game outcome.
     * Logic behind this is based on compareTo for strings.
     * Since P R and S are 112 114 and 115 in unicode, the compareTo will return certain values
     * on player win - these are stored in winCondition variable
     * @param playerChoice player's input
     * @param aiChoice randomized string for AI choice
     * @return outcome string
     */
    private String determineWinner(String playerChoice, String aiChoice) {
        if (playerChoice.equals(aiChoice)) {
            ties++;
            return "ties this round!";
        }
        int gameResult = playerChoice.compareTo(aiChoice);
        for (int a :
                winCondition) {
            if (gameResult == a) {
                wins++;
                return "loses!"; // player wins
            }
        }
        losses++;
        return "wins!"; // player loses

    }
}

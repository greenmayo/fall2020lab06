import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {
    private RpsGame gameOn = new RpsGame();

    /**
     * Method to set the stage
     * @param stage stage object
     */
    public void start(Stage stage) {
        Group root = new Group();
//        creating buttons and text fields
        HBox buttons = new HBox();
        HBox textFields = new HBox();
        Button rock = new Button("Rock");
        Button paper = new Button("Paper");
        Button scissors = new Button("Scissors");
        TextField welcomeMsg = new TextField("Hi Dan!");
        welcomeMsg.setPrefWidth(340);
        TextField wins = new TextField("Wins: 0");
        wins.setPrefWidth(100);
        TextField losses = new TextField("Defeats: 0");
        losses.setPrefWidth(100);
        TextField ties = new TextField("Ties: 0");
        ties.setPrefWidth(100);
//        adding all that to windowLayout
        buttons.getChildren().addAll(rock, paper, scissors);
        textFields.getChildren().addAll(welcomeMsg, wins, losses, ties);
        VBox windowLayout = new VBox();
        windowLayout.getChildren().addAll(textFields,buttons);
        root.getChildren().add(windowLayout);
//        setting all the event handlers
        RpsChoice chooseRock = new RpsChoice(welcomeMsg, wins, losses, ties, "rock", gameOn);
        RpsChoice choosePaper = new RpsChoice(welcomeMsg, wins, losses, ties, "paper", gameOn);
        RpsChoice chooseScissors = new RpsChoice(welcomeMsg, wins, losses, ties, "scissors", gameOn);
        rock.setOnAction(chooseRock);
        paper.setOnAction(choosePaper);
        scissors.setOnAction(chooseScissors);
//        scene is associated with container, dimensions
        Scene scene = new Scene(root, 650, 300);
        scene.setFill(Color.BLACK);

//        associate scene to stage and show

        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}

